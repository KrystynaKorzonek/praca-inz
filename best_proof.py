"""
nowe podejście do dowodów -> najpierw testujemy kilka dowodów na losowej próbce, a potem wybieramy najlepszy z nich do całego dowodu

może okazać się przydatne, jeśli uda się szybciej generować instancje
(obecnie samo dowodzenie jest bardzo szybkie w porównaniu z generowaniem i ta modyfikacja wydłuża czas dowodu, zamiast skracać)
"""

from heapq import nlargest
from random import random
import itertools as it
import time as tm

from gen_inst import all_interesting_vals
from proof_comp import get_one_best_proof_with_name, get_best_proofs_with_names, test_proof
from full_proof import proof_for_given_instances, nice_time


# https://stackoverflow.com/questions/12581437/python-random-sample-with-a-generator-iterable-iterator ==> to dość "szybka" metoda
def sample_from_iterable(it, k):
    return (x for _, x in nlargest(k, ((random(), x) for x in it)))


RANDOM_TRIAL_CNT = 100000 #zależnie od l. wszystkich instancji?




best_proofs, best_proofs_names = get_best_proofs_with_names()

def find_best_proof(instances, problem_params):
    beg = tm.time()
    to_check = list(sample_from_iterable(instances, RANDOM_TRIAL_CNT)) 
    end = tm.time()
    print("czas generowania instancji do sprawdzenia: ", nice_time(end-beg))
    proofs_times = []
    for proof in best_proofs:
        met_done_cnt, time = test_proof(proof, to_check, problem_params)
        proofs_times.append(time)
        print("*", end=" ")
    print()
    for time, name in zip(proofs_times, best_proofs_names):
        print(time, " --> ", name)    
        
    fastest_proof = proof[0]
    fastest_proof_name = best_proofs_names[0]
    min_time = proofs_times[0]
    for proof, time, name in zip(best_proofs[1:], proofs_times[1:], best_proofs_names[1:]):
        if time < min_time:
            min_time = time
            fastest_proof = proof
            fastest_proof_name = name
    return fastest_proof, fastest_proof_name  



def best_proof(n, m, v):
    print("\n**********")
    print(n, m, v)
    beg = tm.time()
    instances = all_interesting_vals(n, m, v)
    instances, instances_cp = it.tee(instances)
    proof, name = find_best_proof(instances_cp, (n, m, v))
    print("Wybrano dowód: " + " -- ".join(name))
    proof_for_given_instances(proof, (n, m, v), instances)
    end = tm.time()
    print("czas całego dowodu: ", nice_time(end-beg))

# best_proof(3, 12, 3)    


