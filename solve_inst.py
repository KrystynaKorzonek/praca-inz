"""
funkcje rozwiązujące instancje 
dane - n, vals(tablice graczy długości m), item_tabs, aims //dodane item_tabs, ze względu na Cheapa
zwracają boola - czy znalazły rozwiązanie
"""

from pulp import *
from collections import deque
import math


def find_division_lp(n, vals, item_tabs, aim_tab):
    m = len(vals[0]) #l.itemów  
    problem = LpProblem("find_division", LpMaximize)
    problem += 0, "dummy objective function"
    x = [[None for item in range(m)] for pl in range(n)]        
    for pl in range(n):
        for item in range(m):
            name = f'x_{pl}_{item}'
            x[pl][item] = LpVariable(name, cat="Binary")
    # item bierze dokładnie 1 gracz:
    for item in range(m):
        problem += lpSum([x[pl][item] for pl in range(n)]) == 1
    # każdy gracz dostaje swoją wartość z tablicy aim_tab
    for pl in range(n):
        earned_val = [x[pl][item] * vals[pl][item] for item in range(m)]
        problem += lpSum(earned_val) >= aim_tab[pl]   
    problem.solve(PULP_CBC_CMD(msg=False))
    return (LpStatus[problem.status] == "Optimal")



def get_pers(n):
    if n==3:
        return [(2, 1, 0), (1, 2, 0), (0, 2, 1), (2, 0, 1), (0, 1, 2), (1, 0, 2)]
    else:
        return it.permutations(range(n))

def get_jump_half_per(n):
    if n == 3:
        pers = [(2, 1, 0), (1, 2, 0), (0, 2, 1), (2, 0, 1), (0, 1, 2), (1, 0, 2)]
    else:
        pers = list(it.permutations(range(n)))    
    return pers[::2]
    
def first_per(n):
    return range(n)  
    
def best_per(n):
    if n==3:
        return (2, 1, 0)
    else:
        return range(n)       



def find_division_round_robin_first(n, vals, item_tabs, aims):
    return find_division_round_robin_from_queue(vals, aims, deque(first_per(n)))
    
def find_division_round_robin_best(n, vals, item_tabs, aims):
    return find_division_round_robin_from_queue(vals, aims, deque(best_per(n)))
    
def find_division_round_robin_all(n, vals, item_tabs, aims):
    return find_division_round_robin_per(vals, aims, math.factorial(n))   
    
def find_division_round_robin_half(n, vals, item_tabs, aims):
    return find_division_round_robin_per(vals, aims, math.factorial(n)//2) 
    
def find_division_round_robin_jumping_half(n, vals, item_tabs, aims):
    for per in get_jump_half_per(n):
        if find_division_round_robin_from_queue(vals, aims, deque(per)):
            return True
    return False    
   
# waiting_players -> kolejka graczy (liczb z <0, .., n-1>)
# kolejność, w jakiej obsługiwani są gracze     
def find_division_round_robin_from_queue(vals, aims, waiting_players):    
    n = len(vals)
    m = len(vals[0])
    earned = [0] * n
    for item in range(m-1, -1, -1):
        if not waiting_players:
            return True
        pl = waiting_players.popleft()
        earned[pl] += vals[pl][item]
        if earned[pl] < aims[pl]:
            waiting_players.append(pl)            
    return not waiting_players # jeśli pusta kolejka, udało się
    
# per_trials -> ile najlepszych permutacji (ztablicowanych w get_pers) sprawdzamy     
def find_division_round_robin_per(vals, aims, per_trials):
    n = len(vals)
    pers = get_pers(n)
    for per in pers[:per_trials]:
        waiting_players = deque()
        for pl in per:
            if aims[pl] > 0:
                waiting_players.append(pl)              
        if find_division_round_robin_from_queue(vals, aims, waiting_players):
            return True
    return False     

  
     
