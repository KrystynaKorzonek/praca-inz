import itertools as it
from scipy.special import binom

def problem_size(n, m, v): #v- max v
    x = binom(m+v, v)
    return binom(n-1+x, n)

# generator list długości k - x1, .., xk, x1 + ... + xk = n
# dopuszczamy puste przydziały (wart x_i = 0)
def get_partitions(n, k):
    for mid_seps in it.combinations_with_replacement(range(n+1), k-1):
        seps = (0,) + mid_seps + (n,)
        partition = []
        for beg, end in zip(seps[:-1], seps[1:]):
            partition.append(end-beg)
        yield partition         
       
# generowanie możliwych wartości gracza       
# n przekazujemy jako argument tylko na użytek filtrowania (czy wystarczy 1 item)
def generate_possible_vals(n, m, max_val):
    for part in get_partitions(m, max_val+1):
       if not wants_one_item(part, n):
            yield part

def generate_instances(n, pos_val_cnt):    
    return get_partitions(n, pos_val_cnt)


def get_partition_number(n, k):
    return int(binom(n+k-1, k-1))    
    
# generowanie wszystkich instancji 
# (bez tych z graczami, którym wystarczy 1 item)
def all_vals(n, m, max_val):      
    def create_item_tabs(instance):
        players_item_tabs = []
        for pl_cnt, pl_type in zip(instance, possible_vals):
            for i in range(pl_cnt):
                players_item_tabs.append(pl_type)
        return players_item_tabs    
        
    possible_vals = list(generate_possible_vals(n, m, max_val))
    instances = generate_instances(n, len(possible_vals))
    all_vals_cnt = 0
    for instance in instances:
        yield(create_item_tabs(instance))
 
 
def wants_one_item(pl_item_tab, n): 
    length = len(pl_item_tab)
    total = sum([val*cnt for val, cnt in zip(range(length), pl_item_tab)])
    wanted = total//n
    best_item = length - 1
    while pl_item_tab[best_item] == 0:
        best_item -= 1
    return best_item >= wanted
    
def all_interesting_vals(n, m, max_val):
    for item_tabs in all_vals(n, m, max_val):
        # wykrywanie przedmiotu, który wszyscy cenią na 0 <=> każdy ma >0 takich o wartości 0
        dump_item_exist = True
        for pl_item_tab in item_tabs:
            if pl_item_tab[0] == 0:
                dump_item_exist = False
                break
        if dump_item_exist:
            continue
        # wykrywanie, że nikt nie ma najcenniejszego przedmiotu (ktoś ma -> instancja jest interesująca)
        for pl_item_tab in item_tabs:
            if pl_item_tab[max_val] > 0:
                yield item_tabs
                break
        
    
    
