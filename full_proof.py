from time import time
from random import randint, choice, sample
from scipy.special import binom

from calc_mms import calc_mms_upper_bound_tab, calc_mms_tab, item_tabs_to_vals
from gen_inst import all_interesting_vals
from solve_inst import find_division_lp, find_division_round_robin_first, find_division_round_robin_best, find_division_round_robin_all, find_division_round_robin_half, find_division_round_robin_jumping_half
from cheap_robin import find_division_cheap_robin_first, find_division_cheap_robin_best, find_division_cheap_robin_all
from proof_comp import get_one_best_proof_with_name


# dowód dla danych instancji (wypisuje czas i liczbę rozwiązanych instancji)
def proof_for_given_instances(method_list, problem_params, instances):
    success_cnt = 0
    inst_cnt = 0
    n, m, max_v = problem_params
    beg = time()
    for inst in instances:
        inst_cnt += 1
        vals = item_tabs_to_vals(inst)
        avgs = None
        mms = None
        avgs = calc_mms_upper_bound_tab(n, vals)
        for method in method_list:                          
            f, use_avg = method
            if not use_avg and not mms:
                mms = calc_mms_tab(n, inst)
            if use_avg:
                aims = avgs
            else:
                aims = mms
            if f(n, vals, inst, aims):
                success_cnt += 1
                break 
    end = time()  
    total_time = end - beg
    print(f'{success_cnt}/{inst_cnt}')
    print(nice_time(total_time))
    return (success_cnt, total_time) 

# dowód dla klasy
def proof(method_list, problem_params):
    n, m, v = problem_params
    instances = all_interesting_vals(n, m, v)
    proof_for_given_instances(method_list, problem_params, instances)


def problem_size(n, m, v): #v- max v
    x = binom(m+v, v)
    return int(binom(n-1+x, n))

def nice_time(sec):
    h = sec//3600
    rest = sec - h*3600
    m = rest//60
    s = rest - m*60
    return(f'{int(h)}h:{int(m)}m:{int(s)}s')

# porównanie całych dowodów (raczej go nie używamy)
def comp_full_proofs(list_of_method_lists, method_names, problem_params):
    n, m, v_max = problem_params
    inst_cnt = problem_size(n, m, v_max)
    for method_list, method_name in zip(list_of_method_lists, method_names):
        print("\n", method_name)
        proof(method_list, problem_params)
      
      
    
    
"""
przeprowadzenie właściwych dowodów
"""   


super_proof, super_proof_name = get_one_best_proof_with_name()

problems = [(3, m, 3) for m in range(8, 15)]
for problem in problems:
    print("\n***")
    print(problem)
    proof(super_proof, problem)
    




