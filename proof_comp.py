from time import time
from random import randint, choice, sample

from calc_mms import calc_mms_upper_bound_tab, calc_mms_tab, item_tabs_to_vals
from gen_inst import all_interesting_vals
from solve_inst import find_division_lp, find_division_round_robin_first, find_division_round_robin_best, find_division_round_robin_all, find_division_round_robin_half, find_division_round_robin_jumping_half
from cheap_robin import find_division_cheap_robin_first, find_division_cheap_robin_best, find_division_cheap_robin_all

# test dowodu, zwraca wyniki jego kolejnych metod
# uwaga, założenie co do dowodu
# --> jeśli używamy średniej, to na początku (jak już policzymy MMSy, to z tego korzystamy)

def test_proof(method_list, inst_to_check, problem_params):
    met_cnt = len(method_list)
    met_done_cnt = [0] * met_cnt
    n, m, max_v = problem_params
    start_with_avg = method_list[0][1] # czy na początku = czy w ogóle średnia
    beg = time()
    for inst in inst_to_check:
        vals = item_tabs_to_vals(inst)
        avgs = None
        mms = None
        if start_with_avg:
            avgs = calc_mms_upper_bound_tab(n, vals)
        for method, nr in zip(method_list, range(met_cnt)):                          
            f, use_avg = method
            if not use_avg and not mms:
                mms = calc_mms_tab(n, inst)
            if use_avg:
                aims = avgs
            else:
                aims = mms
            if f(n, vals, inst, aims):
                met_done_cnt[nr] += 1
                break 
    end = time()  
    total_time = end - beg
    return (met_done_cnt, total_time) 

# opakowanie dla funkcji testującej dowody; wypisuje wyniki    
def test_proof_print(method_list, method_names_list, cnt, to_check, problem_params, print_cnt): 
    print("TEST: ", " -- ".join(method_names_list))
    if print_cnt:
        print("cnt=", cnt) 
        print(problem_params)      
    met_done_cnt, total_time = test_proof(method_list, to_check, problem_params) 
    print("czas:     ", total_time)
    print("czas/ins: ", total_time/cnt)
    for met_name, met_res in zip(method_names_list, met_done_cnt):
        print(met_name, " --> ", met_res, "   ", met_res/cnt)
        
# szybki test jednego dowodu naraz
def random_test_proof_print(method_list, method_names_list, cnt, problem_params):   
    n, m, max_v = problem_params 
    instances = list(all_interesting_vals(n, m, max_v))
    to_check = sample(instances, cnt)
    test_proof_print(method_list, method_names_list, cnt, to_check, problem_params, True)

  
# porównanie dowodów dla cnt losowych instancji        
def comp_proof_test(list_of_method_list, method_names_list, cnt, problem_params):
    print("PORÓWNANIE DOWODÓW:")
    print(problem_params)
    print("cnt=", cnt)
    n, m, max_v = problem_params 
    instances = list(all_interesting_vals(n, m, max_v))
    to_check = sample(instances, cnt)
    for method_list, method_name in zip(list_of_method_list, method_names_list):
        print("\n************") 
        test_proof_print(method_list, method_name, cnt, to_check, problem_params, False)        
        
# porównanie całych dowodów         
def comp_proof_test_all(list_of_method_list, method_names_list, problem_params):
    print("PORÓWNANIE DOWODÓW:")
    print(problem_params)
    print("cnt=wszystkie")
    n, m, max_v = problem_params 
    instances = list(all_interesting_vals(n, m, max_v))
    to_check = instances
    for method_list, method_name in zip(list_of_method_list, method_names_list):
        print("\n************") 
        test_proof_print(method_list, method_name, len(instances), to_check, problem_params, False)          
       

# dowody ze Zwykłym Robinem: najpierw 1 robin, potem solver ze średnią, potem solver z mms-em
all_robin_avg_mms = [(find_division_round_robin_all, True), (find_division_lp, True), (find_division_lp, False)]    
first_robin_avg_mms = [(find_division_round_robin_first, True), (find_division_lp, True), (find_division_lp, False)]  
best_robin_avg_mms = [(find_division_round_robin_best, True), (find_division_lp, True), (find_division_lp, False)]  
half_robin_avg_mms = [(find_division_round_robin_half, True), (find_division_lp, True), (find_division_lp, False)]  
jump_robin_avg_mms = [(find_division_round_robin_jumping_half, True), (find_division_lp, True), (find_division_lp, False)] 
 
all_robin_avg_mms_names = ["Robin-all-per(avg)", "LP(avg)", "LP(mms)"]
first_robin_avg_mms_names = ["Robin-first(avg)", "LP(avg)", "LP(mms)"]
best_robin_avg_mms_names = ["Robin-best(avg)", "LP(avg)", "LP(mms)"]
half_robin_avg_mms_names = ["Robin-half(avg)", "LP(avg)", "LP(mms)"]
jump_robin_avg_mms_names = ["Robin-jump-half(avg)", "LP(avg)", "LP(mms)"]


# dowody z Oszczędnym Robinem (i ewentualnie Zwykłym)
first_cheap_robin_avg_mms = [(find_division_cheap_robin_first, True), (find_division_lp, True), (find_division_lp, False)]
best_cheap_robin_avg_mms = [(find_division_cheap_robin_best, True), (find_division_lp, True), (find_division_lp, False)]
best_normal_best_cheap_avg_mms = [(find_division_round_robin_best, True), (find_division_cheap_robin_best, True), (find_division_lp, True), (find_division_lp, False)]
all_normal_best_cheap_avg_mms = [(find_division_round_robin_all, True), (find_division_cheap_robin_best, True), (find_division_lp, True), (find_division_lp, False)]

first_cheap_robin_avg_mms_names = ["Cheap-Robin-first(avg)", "LP(avg)", "LP(mms)"]
best_cheap_robin_avg_mms_names = ["Cheap-Robin-best(avg)", "LP(avg)", "LP(mms)"]
best_normal_best_cheap_avg_mms_names = ["Round-Robin-best(avg)", "Cheap-Robin-best(avg)", "LP(avg)", "LP(mms)"]
all_normal_best_cheap_avg_mms_names = ["Round-Robin-all(avg)", "Cheap-Robin-best(avg)", "LP(avg)", "LP(mms)"]


# metody, których nie testujemy na (3, 8, 3) - zawierają oszczędnego robina na wszystkich per.
all_cheap_avg_mms = [(find_division_cheap_robin_all, True), (find_division_lp, True), (find_division_lp, False)]
best_normal_all_cheap_avg_mms = [(find_division_round_robin_best, True), (find_division_cheap_robin_all, True), (find_division_lp, True), (find_division_lp, False)]
all_normal_all_cheap_avg_mms = [(find_division_round_robin_all, True), (find_division_cheap_robin_all, True), (find_division_lp, True), (find_division_lp, False)]
all_cheap_avg_mms_names = ["Cheap-Robin-all(avg)", "LP(avg)", "LP(mms)"]
best_normal_all_cheap_avg_mms_names = ["Round-Robin-best(avg)", "Cheap-Robin-all(avg)", "LP(avg)", "LP(mms)"]
all_normal_all_cheap_avg_mms_names = ["Round-Robin-all(avg)", "Cheap-Robin-all(avg)", "LP(avg)", "LP(mms)"]

#zbiorczo listy

robins = [first_robin_avg_mms, best_robin_avg_mms, all_robin_avg_mms, half_robin_avg_mms, jump_robin_avg_mms]
robins_names = [first_robin_avg_mms_names, best_robin_avg_mms_names, all_robin_avg_mms_names, half_robin_avg_mms_names, jump_robin_avg_mms_names]

cheaps = [first_cheap_robin_avg_mms, best_cheap_robin_avg_mms, best_normal_best_cheap_avg_mms, all_normal_best_cheap_avg_mms]
cheap_names = [first_cheap_robin_avg_mms_names, best_cheap_robin_avg_mms_names, best_normal_best_cheap_avg_mms_names, all_normal_best_cheap_avg_mms_names]

#dodatkowe do przectestowania dla m>8 (dla m=8 Cheap-best rozwiązuje tyle instancji co Cheap-all)
cheaps_vars = [all_cheap_avg_mms, best_normal_all_cheap_avg_mms, all_normal_all_cheap_avg_mms]
cheap_vars_names = [all_cheap_avg_mms_names, best_normal_all_cheap_avg_mms_names, all_normal_all_cheap_avg_mms_names]


alls = robins + cheaps
alls_names = robins_names + cheap_names

# dowody bez LP(mms) -- korzystamy z tego, że we wszystkich innych jest przedostatni
alls_no_mid = [proof[:-2] + [proof[-1]] for proof in alls]
alls_no_mid_names = [name[:-2] + [name[-1]] for name in alls_names]


# dla m> 8 (odrzucamy całkiem bez sensu metody, skupiamy się na tych, w które wierzymy = 
alls_m = robins[1:] + cheaps + cheaps_vars
alls_m_names = robins_names[1:] + cheap_names + cheap_vars_names
alls_m_no_mid = [proof[:-2] + [proof[-1]] for proof in alls_m]
alls_m_no_mid_names = [name[:-2] + [name[-1]] for name in alls_m_names]



# funkcje zwracające dowody, które najlepiej wypadły w testach
def get_one_best_proof_with_name():
    return best_normal_all_cheap_avg_mms, best_normal_all_cheap_avg_mms_names

def get_best_proofs_with_names():
    with_lp_avg_proofs = [best_normal_best_cheap_avg_mms, all_normal_best_cheap_avg_mms, best_normal_all_cheap_avg_mms, all_normal_all_cheap_avg_mms]
    with_lp_avg_names = [best_normal_best_cheap_avg_mms_names, all_normal_best_cheap_avg_mms_names, best_normal_all_cheap_avg_mms_names, all_normal_all_cheap_avg_mms_names]
    no_lp_avg_proofs = [proof[:-2] + [proof[-1]] for proof in with_lp_avg_proofs]
    no_lp_avg_names = [name[:-2] + [name[-1]] for name in with_lp_avg_names]
    return (with_lp_avg_proofs + no_lp_avg_proofs, with_lp_avg_names + no_lp_avg_names)

"""
uruchamianie testów: 
"""

# comp_proof_test(alls, alls_names, 10000, (3, 8, 3))
#comp_proof_test(alls_m+alls_m_no_mid, alls_m_names+alls_m_no_mid_names, 10000, (3, 10, 3))


