from pulp import *
import itertools as it

# obliczanie mmsa dla pojedynczego gracza
# items[i] - ile mamy itemów o wartości i
def calc_mms(n, items):
    k = len(items)
    problem = LpProblem("calculate_mms", LpMaximize)
    mms = LpVariable("mms", cat="Integer")
    problem += mms # funkcja celu
    x = [[None for j in range(n)] for i in range(k)]
    for it_val in range(k):
        for pl_num in range(n):
            name = f'x_{it_val}_{pl_num}'
            x[it_val][pl_num] = LpVariable(name, 0, cat="Integer")
    # więzy zapewniające, że rozdamy wszystkie przedmioty (nie więcej!)
    for it_val in range(k):
        problem += lpSum(x[it_val]) == items[it_val]
    # ustawienie zmiennej mms na minimum zdobyte przez gracza
    for pl_num in range(n):
        problem += mms <= lpSum([val*x[val][pl_num] for val in range(k)])         
    problem.solve(PULP_CBC_CMD(msg=False))    
    res = [[x[i][j].value() for j in range(n)] for i in range(k)]
    return mms.value()    
   
    
# vals - lista wartości długości m (wartości m przedmiotów  
def calc_mms_upper_bound(n, vals):
    return sum(vals)//n

def item_tabs_to_vals(item_tabs):
    n = len(item_tabs)
    vals = [None for pl in range(n)]
    for pl in range(n):
        items = item_tabs[pl]
        length = len(items)        
        values = list(it.chain(*[[val]*cnt for val, cnt in zip(range(length), items)]))
        vals[pl] = values
    return vals
    
def calc_avgs_from_vals(item_tabs):
    n = len(item_tabs)
    return [calc_mms_upper_bound(n, item_tabs[pl]) for pl in range(n)]
    

def calc_mms_tab(n, item_tabs):
    return [calc_mms(n, item_tab) for item_tab in item_tabs]

def calc_mms_upper_bound_tab(n, vals_tab):
    return [calc_mms_upper_bound(n, vals) for vals in vals_tab]



    
