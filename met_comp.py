from random import randint, choice, sample
from time import time

from calc_mms import calc_mms_upper_bound_tab, calc_mms_tab, item_tabs_to_vals
from gen_inst import all_interesting_vals
from solve_inst import find_division_lp, find_division_round_robin_first, find_division_round_robin_best, find_division_round_robin_all, find_division_round_robin_half, find_division_round_robin_jumping_half
from cheap_robin import find_division_cheap_robin_first, find_division_cheap_robin_best, find_division_cheap_robin_all



def test_method(method, inst_to_check, problem_params): 
    f, use_avg = method
    if use_avg:
        return test_method_avg(f, inst_to_check, problem_params)
    else:
        return test_method_mms(f, inst_to_check, problem_params)
        
      
def test_method_avg(f, inst_to_check, problem_params):    
    n, m, max_v = problem_params
    success_cnt = 0
    beg = time()
    for inst in inst_to_check:
        vals = item_tabs_to_vals(inst)
        aims = calc_mms_upper_bound_tab(n, vals)
        if f(n, vals, inst, aims):
            success_cnt += 1        
    end = time()
    total_time = end-beg    
    return (success_cnt, total_time)
    
def test_method_mms(f, inst_to_check, problem_params):    
    n, m, max_v = problem_params
    success_cnt = 0
    beg = time()
    for inst in inst_to_check:
        vals = item_tabs_to_vals(inst)
        aims = calc_mms_tab(n, inst)
        if f(n, vals, inst, aims):
            success_cnt += 1        
    end = time()
    total_time = end-beg    
    return (success_cnt, total_time)    
    
    
def random_test_method_print(method, method_name, cnt, problem_params):   
    n, m, max_v = problem_params 
    instances = list(all_interesting_vals(n, m, max_v))
    to_check = sample(instances, cnt)
    test_method_print(method, method_name, cnt, to_check, problem_params, True)
   
# funkcja testującą pojedynczą metodę - na podanych instancjach = to_check    
# to_check to lista lub generator -> podajemy cnt=długosć to check   
def test_method_print(method, method_name, cnt, to_check, problem_params, print_cnt): 
    print("TEST: ", method_name)
    if print_cnt:
        print("cnt=", cnt)   
    success_cnt, total_time = test_method(method, to_check, problem_params)
    time_per_inst = total_time/cnt
    success_ratio = success_cnt/cnt
    print("sukcesy:     ", success_cnt)
    print("wsp sukcesu: ", success_ratio)
    print("czas/ins:  : ", time_per_inst)

# porównanie różnych metod - na tych samych instancjach    
def comp_method_test(method_list, method_names_list, cnt, problem_params):
    print("PORÓWNANIE METOD:")
    print("cnt=", cnt)
    n, m, max_v = problem_params 
    instances = list(all_interesting_vals(n, m, max_v))
    to_check = sample(instances, cnt)
    for method, method_name in zip(method_list, method_names_list):
        print("\n************") 
        test_method_print(method, method_name, cnt, to_check, problem_params, False)
        
    
def nice_time(sec):
    h = sec//3600
    rest = sec - h*3600
    m = rest//60
    return(f'{int(h)}h:{int(m)}m')        
    

# metody z LP-Solverem   
lp_method_list = [(find_division_lp, True), (find_division_lp, False)]
lp_names_list = ["LP(avg)", "LP(mms)"]
   
# sprawdzanie dla zwykłego/oszczędnego robina 1 permutacji, najlepszej i wszystkich    
robin_method_list = [(find_division_round_robin_first, True), (find_division_round_robin_best, True), (find_division_round_robin_all, True)]
robin_names_list = ["Robin-first(avg)", "Robin-best(avg)", "Robin-all-pers(avg)"]

cheap_method_list = [(find_division_cheap_robin_first, True), (find_division_cheap_robin_best, True), (find_division_cheap_robin_all, True)]
cheap_names_list = ["Cheap-Robin-first(avg)", "Cheap-Robin-best(avg)", "Cheap-Robin-all-pers(avg)"]

# metody sprawdzające pierwsze 3 permutacje; oraz 1., 3. i 5.
extra_robin_method_list = [(find_division_round_robin_half, True), (find_division_round_robin_jumping_half, True)]
extra_robin_method_names = ["Robin-half(avg)", "Robin-jump(avg)"]


all_basic_robin_method_list = robin_method_list + cheap_method_list
all_basic_robin_method_names = robin_names_list + cheap_names_list

all_robin_method_list = robin_method_list + extra_robin_method_list + cheap_method_list
all_robin_method_names = robin_names_list + extra_robin_method_names + cheap_names_list

all_method_list = lp_method_list + all_robin_method_list
all_method_names = lp_names_list + all_robin_method_names

#comp_method_test(all_method_list, all_method_names, 10000, (3, 8, 3))
#comp_method_test(robin_method_list, robin_names_list, 10000, (3, 8, 3))
comp_method_test(all_robin_method_list, all_robin_method_names, 10000, (3, 8, 3))




    
