from collections import deque
from calc_mms import calc_avgs_from_vals, item_tabs_to_vals
import math
from solve_inst import get_pers, first_per, best_per

# zwraca dla tablicy wartościowań tablicę kolejek
# każdy gracz ma w swojej kolejce itemy, w kolejności od najbardziej pożądanego
# +altruistycznie sporód itemów o równej wartości wybiera item o najmniejszym numerze
# np. dla vals = [[0, 0, 2, 4, 4, 4, 4]]
#     zwraca [Queue[3, 4, 5, 6, 2, 0, 1]]
def make_item_queue_tab_from_item_tabs(item_tabs, m):
    n = len(item_tabs)
    res = []
    for pl_item_tab in item_tabs:
        pl_queue = deque()
        first_added_item = m
        for item_cnt in pl_item_tab[:0:-1]: #0--> pomijamy 1 wart, nie kolejkujemy itemów o wart 0
            first_added_item -= item_cnt
            for i in range(item_cnt):
                pl_queue.append(first_added_item + i)
        res.append(pl_queue)        
    return res    
  
# kolejka graczy waiting_players - gracze, którym brakuje dodatniej wartości
def find_division_cheap_robin_from_queue(n, item_tabs, vals, aims, waiting_players):  
    m = len(vals[0])
    earned = [0] * n
    allocated = [False] * m  
    allocated_cnt = 0
    item_queue_tab = make_item_queue_tab_from_item_tabs(item_tabs, m)
    while allocated_cnt < m:
        if not waiting_players:
            return True
        pl = waiting_players.popleft()
        pl_queue = item_queue_tab[pl]
        if not pl_queue:
            return False
        considered_item = pl_queue.popleft()
        while allocated[considered_item] == True:
            if not pl_queue:
                return False
            considered_item = pl_queue.popleft()
        # do testów (np. za pomocą funkcji cheap_robin_simple_test
        # print(f'gracz {pl} bierze {considered_item}')
        allocated[considered_item] = True
        allocated_cnt += 1
        earned[pl] += vals[pl][considered_item]
        if earned[pl] < aims[pl]:
            waiting_players.append(pl)   
    return not waiting_players #kolejka pusta = wszyscy gracze szczęśliwi   

    
def find_division_cheap_robin_per_trials(n, item_tabs, vals, aims, per_trials):
    pers = get_pers(n)
    for per in pers[:per_trials]:               
        if find_division_cheap_robin_from_per(n, item_tabs, vals, aims, per):
            return True
    return False       
    

def find_division_cheap_robin_first(n, vals, item_tabs, aims):
    return find_division_cheap_robin_from_per(n, item_tabs, vals, aims, first_per(n))


def find_division_cheap_robin_best(n, vals, item_tabs, aims):
    return find_division_cheap_robin_from_per(n, item_tabs, vals, aims, best_per(n))    
    

def find_division_cheap_robin_all(n, vals, item_tabs, aims):
    return find_division_cheap_robin_per_trials(n, item_tabs, vals, aims, math.factorial(n))    
    

def find_division_cheap_robin_from_per(n, item_tabs, vals, aims, per): 
    waiting_players = deque()
    for pl in per:
        if aims[pl] > 0:
            waiting_players.append(pl)              
    if find_division_cheap_robin_from_queue(n, item_tabs, vals, aims, waiting_players):
        return True
    return False       
    
# f. tylko do testów    
def cheap_robin_simple_test(item_tabs, per):
    n = len(item_tabs)
    vals = item_tabs_to_vals(item_tabs)  
    aims = calc_avgs_from_vals(vals)    
    waiting_players = deque()
    for pl in per:
        if aims[pl]>0:
            waiting_players.append(pl) 
    for pl_row, aim in zip(vals, aims):
        print(pl_row, "-->", aim)
    return find_division_cheap_robin_from_queue(n, item_tabs, vals, aims, waiting_players)




