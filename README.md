Program napisany na użytek pracy inżynierskiej "Dyskretna sprawiedliwość, czyli sprawiedliwy podział wg kryterium Maximin Share". 
Przeprowadza komputerowe dowody istnienia sprawiedliwych podziałów w pewnych prostych przypadkach - dla ustalonej liczby graczy, przedmiotów i maksymalnej wartości przedmiotu (wartości przedmiotów są całkowite). 

### dowodzenie
Do przeprowadzenia dowodu służy funkcja proof z pliku full_proof.py.
Metoda dowodzenia zaimplementowana w pliku best_proof.py jest nieefektywna (przyda się, jeśli uda się generować instancje szybciej niż przeprowadzać same dowody).

### biblioteki
Program wykorzystuje bibliotekę PuLP (https://coin-or.github.io/pulp/)
 



